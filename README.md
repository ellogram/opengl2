# Résumé
Projet OpenGL de scène réaliste : scène extérieure avec lumière dynamique réaliste, et déplacement clavier libre dans la scène.

# Auteurs
Mathieu Margollé, Alice Gonnaud

# Dépôt du code
Le code est déposé sur GitLab à l'adresse suivante :

https://gitlab.com/ellogram/opengl2/

# Lancer le projet

**La première compilation peut prendre quelques temps. L'exécution prend plusieurs secondes. Il se peut que vous soyez invités à quitter car le programme semble ne pas répondre. Veuillez patienter, et cliquer sur *Attendre* si vous êtes invités à quitter.**

NB : La variation de lumière peut manquer de fluidité et le déplacement peut être difficile si la machine a des performances limitées.

## Dépendances 
Notre programme fait appel aux bibliothèques suivantes :
* ASSIMP (v 3.1)
* GLAD
* GLFW (v 3.2.1)
* GLM (v 0.9.5)
* STB 

Comme spécifié, ces bibliothèques ne sont pas fournies avec le code. 

**Veillez à ajouter les bibliothèques requises au dossier *Vendor* avant compilation.** 

NB : Les bibliothèques (répertoire _Vendor_ entier) sont disponibles sur le GitLab du projet : https://gitlab.com/ellogram/opengl2/ .

## Exécuter
**Copier le répertoire *Resources* dans le répertoire de compilation (Release) avant compilation.**

Compiler et exécuter le main.cpp.

## Se déplacer dans la scène
Le déplacement se fait par clavier :

ZQSD pour la translation :
* Z vers l'avant
* S vers l'arrière
* Q vers la gauche
* D vers la droite

Les flèches pour la rotation (haut, bas, gauche, droite).

Echap permet de quitter la scène.


# Description du livrable

* Common : Les fichiers sources que nous avons implémentés (ou modifiés en partant des sources fournies ou de sources récupérées, voir _Références_) sont :
    * main. cpp : entrée du programme : gestion de la scène, de la fenêtre...
    * Camera.h : gestion des inputs et du déplacement dynamique dans le modèle
    * Shader.h, Model.h, Mesh.h : gestion de l'import des objets et de la communication entre les librairies, le main et les shaders
    * sun.h, atmosphere.h, atmosphere.cpp : calcul de la position du soleil et de la couleur du ciel

* Resources/Shaders : Les shaders que nous avons implémentés sont :
    * background : couleur du ciel
    * model : objets de la scène
    * model_100x : objets générés aléatoirement (arbres en arrière-plan, voir _Génération aléatoire_)
    * obj_norm : objets avec normal map pour un rendu en relief (ici, le cottage)

* Resources/Models : Objets de la scène et textures : sol, cottage, lampadaire, arbres, buissons.

* Examples : images de la scène modélisée (reprises ici, voir _Quelques illustrations_)
* Vendor : __AJOUTER LES BIBLIOTHEQUES ICI__



# Modules implémentés

## Déplacement dans le monde
La caméra (pour le déplacement dans la scène) a été modifiée pour prendre en entrée les contrôles clavier (et non souris). Cela permet une meilleure gestion du déplacement, surtout si les performances de la machine sont limitées.

## Création du monde
Nous avons choisi et téléchargé les modèles (objets .obj : arbres, sol, maison...). Nous les avons en partie personnalisés (sur blender notamment) avant import dans notre code, pour apporter des corrections de formes et de textures. Nous les importons alors dans le code où nous les positionnons dans le monde et nous les traitons (texture, lumière).

Nous avons refait les méthodes de texturation pour prendre en compte la normale, pour ajouter un effet d'ombre selon l'orientation de la face et la position de la lumière. Les images de texture (couleurs) et les images normal map sont importées. Nous avons implémenté les traitements de la lumière utilisant ces images de base (prise en compte des images couleur et application des normales) pour apporter une impression de relief et de texture réaliste (dans les shaders, et en partie dans les classes Model et Mesh utilisant la libraire ASSIMP). Nous avons également implémenté l'import des propriétés des matériaux depuis les objets (modification des classes Model, Mesh communiquant avec ASSIMP).

## Génération aléatoire
La scène centrale (position caméra initiale) a été installée à la main dans le code. Le sol s'étend beaucoup plus largement pour donner un effet de lointain, et les bords du décor ont été façonnés pour former un horizon plus réaliste avec des montagnes. 
Nous avons implémenté la génération aléatoire d'arbres autour de notre scène centrale, pour peupler ce décor étendu vers l'horizon, et parfaire l'impression de décor sans limites bornées.

## Source de lumière fixe
La lumière du lampadaire diffuse une lumière immobile de nuit. Nous avons implémenté son éclairage, qui prend une direction privilégiée depuis la position de l'ampoule (ombre de la lampe sur le sol), et prend en compte la distance et l'angle avec les objets.

## Cycle dynamique jour - nuit
Les astres, sources de lumière éclairant la scène, parcourent le ciel : le soleil durant le cycle jour, la lune la nuit.
Ils diffusent une lumière dans le ciel qui change en fonction de "l'heure" (instant du cycle). Les dégradés de couleur dans le ciel et la lumière ambiante de la scène changent ainsi tout au long des deux cycles. Les effets dans le ciel, et de la lumière ambiante de la scène touchant les objets, sont hautement réalistes et sont dynamiques (ils évoluent en taille, intensité ou couleur au cours du temps) : halo diffus autour de l'astre, dégradé de couleur non linéaire dans le ciel (horizon...), intensité (i.e. plus ou moins lumineux selon le moment), température (i.e. tirant plus ou moins sur le blanc (midi), le rouge (soir) ou le bleu (nuit)...), etc. Nous avons également ajouté les étoiles, de luminosité variée.

Nous avons implémenté la lumière à partir de modèles physiques (voir *Références*).
Le module permet de choisir l'époque de l'année et la latitude : la modélisation s'adapte à la réalité (durée du jour, luminosité...).



# Quelques illustrations

Voici quelques images de la scène balayant diverses heures de la journée.

![8h](/Examples/8h.png) ![12h](/Examples/12h.png)
![16h](/Examples/16h.png) ![18h](/Examples/18h.png)
![19h](/Examples/19h.png) ![20h](/Examples/20h.png)
![23h](/Examples/23h.png) ![24h](/Examples/24h.png)


# Références
Pour implémenter certains modules, nous nous sommes appuyés en partie sur des sources existantes, que nous avons reprises et adaptées à notre projet.

* Exemples de codes sources, tutoriels :
    * https://learnopengl.com/
* Structure de base avec les librairies importées en tant que submodules git :
    * https://github.com/Polytonic/Glitter
* Modèles 3D (obj, textures, blend) :
    * https://free3d.com/
    * https://www.turbosquid.com/
    * https://www.cgtrader.com/
* Textures :
    * https://www.textures.com/
* Couleur du ciel :
    * https://github.com/wwwtyro/glsl-atmosphere 
    * Avec le fork https://github.com/hdachev/glsl-atmosphere pour l'ajout de l'effet de l'ozone et des meilleures valeurs de Rayleigh
* Calculs astronomiques (position du soleil) :
    * https://github.com/mourner/suncalc
* Pour l'inspiration et l'obj sol du décor : 
    * https://csc.lsu.edu/~kooima/misc/cs594/final/ 
