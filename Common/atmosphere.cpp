#include <stdio.h>
#include <math.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include "atmosphere.h"

float min(float a, float b) { return a<b?a:b; }

/**
 * Calculate ray-sphere intersection
 * Assume that the sphere is centred at the origin.
 * No intersection when result.x > result.y
 * @param  r0 Ray origin
 * @param  rd Normalised ray direction
 * @param  sr Sphere radius
 * @return A vec2 with the distances. x=minimum, y=maximum.
 */
glm::vec2 rsi(glm::vec3 r0, glm::vec3 rd, float sr) {
    // ray-sphere intersection that assumes
    // the sphere is centered at the origin.
    // No intersection when result.x > result.y
    float a = glm::dot(rd, rd);
    float b = 2.0 * glm::dot(rd, r0);
    float c = glm::dot(r0, r0) - (sr * sr);
    float d = (b*b) - 4.0*a*c;
    if (d < 0.0) return glm::vec2(1e5,-1e5);
    return glm::vec2(
        (-b - sqrt(d))/(2.0*a),
        (-b + sqrt(d))/(2.0*a)
    );
}

float geometricSeries(float commonRatio, float numTerms) {
    // Sum of first n terms in a geometric progression starting with 1:
    // a * ( 1 - r^n ) / ( 1 - r ), here a is 1.
    return (1.0 - pow(commonRatio, numTerms))
         / (1.0 - commonRatio);
}

/**
 * Calculate the color of the sky!
 * If the standard earth values are desired, can be called using:
 *   atmosphere(r0, rd, pSun, ATMOSPHERE_EARTH);
 *
 * @param  rd      Normalized ray direction
 * @param  pSun    Direction of the sun (eg: (vec3){0,0.1,-1})
 * @param  iSun    Intensity of the sun (eg: 22.0)
 * @param  r0      Ray origin (eg: (vec3){0,6372e3,0})
 * @param  rPlanet Radius of the planet in meters (eg: 6371e3)
 * @param  rAtmos  Radius of the atmosphere in meters (eg: 6471e3)
 * @param  kRlh    Rayleigh scattering coefficient (eg: (vec3){5.5e-6, 13.0e-6, 22.4e-6})
 * @param  kMie    Mie scattering coefficient (eg: 21e-6)
 * @param  kOzo    Ozone scattering coefficient
 * @param  shRlh   Rayleigh scale height (eg: 8e3)
 * @param  shMie   Mie scale height (eg: 1.2e3)
 * @param  g       Mie preferred scattering direction (eg: 0.758)
 * @return         sky color
 */
//
glm::vec3 atmosphere(glm::vec3 rd, glm::vec3 pSun, float iSun, glm::vec3 r0, float rPlanet, float rAtmos,
                     glm::vec3 kRlh, float kMie, glm::vec3 kOzo, float shRlh, float shMie, float g) {
    int i,j;

    // Normalize the sun and view directions.
    pSun = normalize(pSun);
    rd = normalize(rd);

    // Calculate the step size of the primary ray.
    glm::vec2 p = rsi(r0, rd, rAtmos);                             // How far in this direction is the edge of the atmosphere?
    if (p.x > p.y) return glm::vec3(0.0f);                         // If the ray will not contact the atmosphere, return BLACK.
    p.y = min(p.y, rsi(r0, rd, rPlanet).x);                        // Does the ray hit the planet? If so, then shorten the distance.
    float iDist = p.y - p.x;
    float iStepSize = iDist / geometricSeries(iStepGrowth, (float)iSteps);

    // Initialize the primary ray time.
    float iTime = 0.0f;

    // Initialize accumulators for Rayleigh and Mie scattering.
    glm::vec3 totalRlh(0.0f);
    glm::vec3 totalMie(0.0f);

    // Initialize optical depth accumulators for the primary ray.
    float iOdRlh = 0.0f;
    float iOdMie = 0.0f;

    // Calculate the Rayleigh and Mie phases.
    float mu = dot(rd, pSun);
    float mumu = mu * mu;
    float gg = g * g;
    float pRlh = 3.0f / (16.0f * M_PI) * (1.0f + mumu);
    float pMie = 3.0f / (8.0f * M_PI) * ((1.0f - gg) * (mumu + 1.0f)) / (pow(1.0f + gg - 2.0f * mu * g, 1.5f) * (2.0f + gg));

    // Sample the primary ray.
    for (i = 0; i < iSteps; i++) {

        if (i != -1) {
            // Calculate the primary ray sample position.
            glm::vec3 iPos = r0 + (iTime + iStepSize * 0.5f) * rd;

            // Calculate the height of the sample.
            float iHeight = length(iPos) - rPlanet;

            // Calculate the optical depth of the Rayleigh and Mie scattering for this step.
            float odStepRlh = exp(-iHeight / shRlh) * iStepSize;
            float odStepMie = exp(-iHeight / shMie) * iStepSize;

            // Accumulate optical depth.
            iOdRlh += odStepRlh;
            iOdMie += odStepMie;

            // Calculate the step size of the secondary ray.
            float jStepSize = rsi(iPos, pSun, rAtmos).y / geometricSeries(jStepGrowth, (float)jSteps);

            // Initialize the secondary ray time.
            float jTime = 0.0f;

            // Initialize optical depth accumulators for the secondary ray.
            float jOdRlh = 0.0f;
            float jOdMie = 0.0f;

            // Sample the secondary ray.
            for (j = 0; j < jSteps; j++) {

                // Calculate the secondary ray sample position.
                glm::vec3 jPos = iPos + pSun * (jTime + jStepSize * 0.5f);

                // Calculate the height of the sample.
                float jHeight = length(jPos) - rPlanet;

                // Accumulate the optical depth.
                jOdRlh += exp(-jHeight / shRlh) * jStepSize;
                jOdMie += exp(-jHeight / shMie) * jStepSize;

                // Increment the secondary ray time.
                jTime += jStepSize;
                jStepSize *= jStepGrowth;
            }

            // Calculate attenuation.
            glm::vec3 attn =
                glm::exp(-(  kMie * (iOdMie + jOdMie) * (float)mieExtinctionMul
                           + kRlh * (iOdRlh + jOdRlh)
                           + kOzo * (iOdRlh + jOdRlh) * (float)ozoMul));

            // Accumulate scattering.
            totalRlh += odStepRlh * attn;
            totalMie += odStepMie * attn;
        }

        // Increment the primary ray time.
        iTime += iStepSize;
        iStepSize *= iStepGrowth;
    }

    // Calculate and return the final color.
    return iSun * (pRlh * kRlh * totalRlh + pMie * kMie * totalMie);
}
