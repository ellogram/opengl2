#ifndef _ATMOSPHERE_H_
#define _ATMOSPHERE_H_
#include <glm/glm.hpp>

/**
 * Define a "standard" earthlike set of parameters to use in atmosphere()
 */
//#define ATMOSPHERE_EARTH (vec3){0,6372e3,0}, 6371e3, 6471e3, (vec3){5.8e-6,13.5e-6,33.1e-6}, 21e-6, (vec3){3.426e-7, 8.298e-7, 0.356e-7}, 8e3, 1.2e3, 0.758
#define iSteps 16
#define jSteps 8
#define iStepGrowth 1.15
#define jStepGrowth 1.15
#define mieExtinctionMul 1.11
#define ozoMul 6.00

float min(float a, float b);

/**
 * Calculate ray-sphere intersection
 * Assume that the sphere is centred at the origin.
 * No intersection when result.x > result.y
 * @param  r0 Ray origin
 * @param  rd Normalised ray direction
 * @param  sr Sphere radius
 * @return A vec2 with the distances. x=minimum, y=maximum.
 */
glm::vec2 rsi(glm::vec3 r0, glm::vec3 rd, float sr);

float geometricSeries(float commonRatio, float numTerms);

/**
 * Calculate the color of the sky!
 * If the standard earth values are desired, can be called using:
 *   atmosphere(r0, rd, pSun, ATMOSPHERE_EARTH);
 *
 * @param  rd      Normalized ray direction
 * @param  pSun    Direction of the sun (eg: (vec3){0,0.1,-1})
 * @param  iSun    Intensity of the sun (eg: 22.0)
 * @param  r0      Ray origin (eg: (vec3){0,6372e3,0})
 * @param  rPlanet Radius of the planet in meters (eg: 6371e3)
 * @param  rAtmos  Radius of the atmosphere in meters (eg: 6471e3)
 * @param  kRlh    Rayleigh scattering coefficient (eg: (vec3){5.5e-6, 13.0e-6, 22.4e-6})
 * @param  kMie    Mie scattering coefficient (eg: 21e-6)
 * @param  kOzo    Ozone scattering coefficient
 * @param  shRlh   Rayleigh scale height (eg: 8e3)
 * @param  shMie   Mie scale height (eg: 1.2e3)
 * @param  g       Mie preferred scattering direction (eg: 0.758)
 * @return         [description]
 */
glm::vec3 atmosphere(glm::vec3 rd, glm::vec3 pSun, float iSun, glm::vec3 r0, float rPlanet, float rAtmos, glm::vec3 kRlh, float kMie, glm::vec3 kOzo, float shRlh, float shMie, float g);

#endif
