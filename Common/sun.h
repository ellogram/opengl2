#ifndef SUN_H
#define SUN_H
#include <cstring>
#include <ctime>
#include <math.h>


static const double dayMs = 1000 * 60 * 60 * 24;
static const double J1970 = 2440588;
static const double J2000 = 2451545;
static const double rad = M_PI / 180;
static const double deg = 180 / M_PI;
static const double e = rad * 23.4397; // obliquity of the Earth
static double toJulian(double msecSinceEpoch) { return msecSinceEpoch / dayMs - 0.5 + J1970; }
static double fromJulian(double j) { return (j + 0.5 - J1970) * dayMs; }
static double toDays(double msecSinceEpoch) { return toJulian(msecSinceEpoch) - J2000; }
static double declination(double l, double b) { return asin(sin(b) * cos(e) + cos(b) * sin(e) * sin(l)); }
static double rightAscension(double l, double b) { return atan2(sin(l) * cos(e) - tan(b) * sin(e), cos(l)); }
static double siderealTime(double d, double lw) { return rad * (280.16 + 360.9856235 * d) - lw; }
static double solarMeanAnomaly(double d) { return rad * (357.5291 + 0.98560028 * d); }
static double eclipticLongitude(double M) {
    double C = rad * (1.9148 * sin(M) + 0.02 * sin(2 * M) + 0.0003 * sin(3 * M)); // equation of center
    double P = rad * 102.9372; // perihelion of the Earth
    return M + C + P + M_PI;
}
static double azimuth(double H, double phi, double dec) { return atan2(sin(H), cos(H) * sin(phi) - tan(dec) * cos(phi)); }
static double altitude(double H, double phi, double dec) { return asin(sin(phi) * sin(dec) + cos(phi) * cos(dec) * cos(H)); }
static void getSunPosition(double &az, double &alt, double epoch, double lat=45.0, double lng=4.0)
{
    double lw  = rad * -lng;
    double phi = rad * lat;
    double d   = toDays(epoch);

    double M = solarMeanAnomaly(d);
    double L = eclipticLongitude(M);

    double dec = declination(L, 0);
    double ra  = rightAscension(L, 0);
    double H   = siderealTime(d, lw) - ra;

    az = azimuth(H, phi, dec);
    alt = altitude(H, phi, dec);
}
static time_t convertTimeToEpoch(const char* theTime, const char* format = "%Y-%m-%d %H:%M:%S")
{
   std::tm tmTime;
   std::memset(&tmTime, 0, sizeof(tmTime));
   strptime(theTime, format, &tmTime);
   return mktime(&tmTime);
}

#endif
