//#define STB_IMAGE_IMPLEMENTATION
#define GLM_ENABLE_EXPERIMENTAL
#define AA

// Local Headers
#include "sun.h"
#include "atmosphere.h"
#include "Shader.h"
#include "Model.h"
#include "Camera.h"

// System Headers
#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/string_cast.hpp>

// Standard Headers
#include <cstdio>
#include <cstdlib>
#include <math.h>
#include <algorithm>
#include <random>

// Window dimensions
const int mWidth = 800;
const int mHeight = 600;


int main() {

    // Load GLFW and Create a Window
    glfwInit();
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
    glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);
#ifdef AA
    glfwWindowHint(GLFW_SAMPLES, 4);
#endif
    auto mWindow = glfwCreateWindow(mWidth, mHeight, "OpenGL", nullptr, nullptr);

    // Check for Valid Context
    if (mWindow == nullptr) {
        fprintf(stderr, "Failed to Create OpenGL Context");
        return EXIT_FAILURE;
    }

    // Create Context and Load OpenGL Functions
    glfwMakeContextCurrent(mWindow);
    gladLoadGL();
    fprintf(stderr, "OpenGL %s\n", glGetString(GL_VERSION));

    glEnable(GL_DEPTH_TEST);
#ifdef AA
    glEnable(GL_MULTISAMPLE);
#endif

    // Vertices creation
    GLfloat vertices[] = {
        /*     Positions    |       Colors       |     Normals     */
        -0.5f, -0.5f, -0.5f,  1.0f,  0.0f,  0.0f,  0.0f,  0.0f, -1.0f,
         0.5f, -0.5f, -0.5f,  1.0f,  0.0f,  0.0f,  0.0f,  0.0f, -1.0f,
         0.5f,  0.5f, -0.5f,  1.0f,  0.0f,  0.0f,  0.0f,  0.0f, -1.0f,
        -0.5f,  0.5f, -0.5f,  1.0f,  0.0f,  0.0f,  0.0f,  0.0f, -1.0f,

        -0.5f, -0.5f,  0.5f,  1.0f,  1.0f,  0.0f,  0.0f,  0.0f,  1.0f,
         0.5f, -0.5f,  0.5f,  1.0f,  1.0f,  0.0f,  0.0f,  0.0f,  1.0f,
         0.5f,  0.5f,  0.5f,  1.0f,  1.0f,  0.0f,  0.0f,  0.0f,  1.0f,
        -0.5f,  0.5f,  0.5f,  1.0f,  1.0f,  0.0f,  0.0f,  0.0f,  1.0f,

        -0.5f,  0.5f,  0.5f,  0.0f,  0.5f,  0.0f, -1.0f,  0.0f,  0.0f,
        -0.5f,  0.5f, -0.5f,  0.0f,  0.5f,  0.0f, -1.0f,  0.0f,  0.0f,
        -0.5f, -0.5f, -0.5f,  0.0f,  0.5f,  0.0f, -1.0f,  0.0f,  0.0f,
        -0.5f, -0.5f,  0.5f,  0.0f,  0.5f,  0.0f, -1.0f,  0.0f,  0.0f,

         0.5f,  0.5f,  0.5f,  0.0f,  0.5f,  0.5f,  1.0f,  0.0f,  0.0f,
         0.5f,  0.5f, -0.5f,  0.0f,  0.5f,  0.5f,  1.0f,  0.0f,  0.0f,
         0.5f, -0.5f, -0.5f,  0.0f,  0.5f,  0.5f,  1.0f,  0.0f,  0.0f,
         0.5f, -0.5f,  0.5f,  0.0f,  0.5f,  0.5f,  1.0f,  0.0f,  0.0f,

        -0.5f, -0.5f, -0.5f,  0.0f,  0.5f,  1.0f,  0.0f, -1.0f,  0.0f,
         0.5f, -0.5f, -0.5f,  0.0f,  0.5f,  1.0f,  0.0f, -1.0f,  0.0f,
         0.5f, -0.5f,  0.5f,  0.0f,  0.5f,  1.0f,  0.0f, -1.0f,  0.0f,
        -0.5f, -0.5f,  0.5f,  0.0f,  0.5f,  1.0f,  0.0f, -1.0f,  0.0f,

        -0.5f,  0.5f, -0.5f,  0.5f,  0.5f,  0.5f,  0.0f,  1.0f,  0.0f,
         0.5f,  0.5f, -0.5f,  0.5f,  0.5f,  0.5f,  0.0f,  1.0f,  0.0f,
         0.5f,  0.5f,  0.5f,  0.5f,  0.5f,  0.5f,  0.0f,  1.0f,  0.0f,
        -0.5f,  0.5f,  0.5f,  0.5f,  0.5f,  0.5f,  0.0f,  1.0f,  0.0f,
    };

    GLshort indices[]{
        0, 1, 2,
        2, 3, 0,

        4, 5, 6,
        6, 7, 4,

        8, 9, 10,
        10, 11, 8,

        12, 13, 14,
        14, 15, 12,

        16, 17, 18,
        18, 19, 16,

        20, 21, 22,
        22, 23, 20
    };

    GLuint EBO, VBO, VAO;
    glGenBuffers(1, &EBO);
    glGenVertexArrays(1, &VAO);
    glGenBuffers(1, &VBO);

    // Bind the Vertex Array Object first, then bind and set vertex buffer(s), and then configure vertex attributes(s).
    glBindVertexArray(VAO);
    glBindBuffer(GL_ARRAY_BUFFER, VBO);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);
    // Bind the elements
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), indices, GL_STATIC_DRAW);
    // position attribute
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 9 * sizeof(GLfloat), (void*)0);
    glEnableVertexAttribArray(0);
    // color attribute
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 9 * sizeof(GLfloat), (void*)(3* sizeof(GLfloat)));
    glEnableVertexAttribArray(1);

    // Shaders

    Shader bgShader("./Resources/Shaders/background.vertexshader", "./Resources/Shaders/background.fragmentshader");
    Shader modelShader("./Resources/Shaders/model.vertexshader", "./Resources/Shaders/model.fragmentshader");
    Shader model100Shader("./Resources/Shaders/model_100x.vertexshader", "./Resources/Shaders/model.fragmentshader");
    Shader objnormShader("./Resources/Shaders/obj_norm.vertexshader", "./Resources/Shaders/obj_norm.fragmentshader");

    // Models

//    Model basictree("./Resources/Models/TreeBasic/tree2.obj");
//    Model ground("./Resources/Models/Ground/forest_ground.obj");
    Model ground("./Resources/Models/Ground/ground_uneven.obj");
    Model lamp("./Resources/Models/Lamp/Lamp.obj");
    Model cottage("./Resources/Models/Cottage/cottage.obj");
    Model tree1("./Resources/Models/3Trees/CL13y.obj");
    Model tree2("./Resources/Models/3Trees/CL13a.obj");
    Model tree3("./Resources/Models/3Trees/CL13m.obj");
    Model bush("./Resources/Models/Bush/bush.obj");

    // Random positions for trees on a grid with clear center

    unsigned int gridSize = 50;
    float halfGridSize = (float)gridSize/2.0f;
    auto rng = std::default_random_engine {};
    std::vector<int> ens(gridSize * gridSize);
    std::iota(ens.begin(), ens.end(), 1.0f);
    std::shuffle(ens.begin(), ens.end(), rng);

    unsigned int numInstances = 100;
    float clearRadius = 120.0f;
    float scalingFactor = 20.0f;
    unsigned int index = 0;
    int i = 0;
    std::vector<glm::vec3> translations(numInstances);
    int size = static_cast<int>(translations.size());
    while (index < size && i < gridSize * gridSize)
    {
        float x = (float)(ens[i] % gridSize) - halfGridSize;
        float y = (float)(ens[i] / gridSize) - halfGridSize;
        x *= scalingFactor;
        y *= scalingFactor;
        i++;
        if (std::abs(x) < clearRadius && std::abs(y) < clearRadius)
            continue;
        translations[index] = glm::vec3(x, 0.0f, y);
        index++;
    }
    model100Shader.use();
    model100Shader.setInstances(numInstances);

    // Camera setup

    glm::vec3 startingPosition(0.0f, 5.0f, 30.0f);
    Camera camera(startingPosition, mWindow);

    // Rendering Loop

    while (glfwWindowShouldClose(mWindow) == false) {

        glfwPollEvents();
        camera.Do_Movement();

        // Background Fill Color
        glClearColor(0.5f, 0.8f, 1.0f, 1.0f);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        glBindVertexArray(VAO);

        // View

        float near = 0.1f;
        float far = 1000.0f;
        float fov = glm::radians(45.0f);
        glm::mat4 projection = glm::perspective(fov, (float)mWidth / (float)mHeight, near, far);
        glm::mat4 view = camera.GetViewMatrix();
        glm::mat4 viewproj = projection * view;
		glm::mat4 model;

        // Sky background

        bgShader.use();
        // Scaling of a cube to fill the far plane, on which we will draw the sky
        float hScale = 2.0f * far * tan(fov/2.0f);
        float wScale = (float)mWidth / (float)mHeight * hScale;
        glm::mat4 scaleMat = glm::scale(glm::mat4(1.0f), glm::vec3(wScale, hScale, 1.0f));
        glm::mat4 rotMat = glm::mat4(glm::transpose(glm::mat3(view)));
        glm::mat4 transMat = glm::translate(glm::mat4(1.0f), camera.Position + camera.Front * far);
        model = transMat * rotMat * scaleMat;
        bgShader.setMat4("model", model);
        bgShader.setMat4("viewproj", viewproj);
        // Date, time and position
        double hour = std::fmod(glfwGetTime(), 45.0);
        double epoch = convertTimeToEpoch("2018-06-21 07:35:30");
        epoch += hour * 60.0 * 60.0;
        double lat = 45.0;
        double lon = 0.0;
        // Sun position
        double az, alt;
        getSunPosition(az, alt, epoch*1000.0, lat, lon);
        glm::vec3 sunPos(cos(alt)*cos(az-M_PI/2.0), sin(alt), -cos(alt)*sin(az+M_PI/2.0));
        bgShader.setVec3("uSunPos", sunPos);
        // Drawing
        glDrawElements(GL_TRIANGLES, 3*12, GL_UNSIGNED_SHORT, 0);

        // Sky and sun color
        glm::vec3 skyColor(0.0f, 0.2f, 0.4f);
        if (sunPos.y > 0.0f) {
            float sunIntensity = 100.0f;
            skyColor = atmosphere(-sunPos, sunPos, sunIntensity,
                                  glm::vec3(0,6372e3,0), 6371e3, 6471e3,
                                  glm::vec3(5.8e-6, 13.5e-6, 33.1e-6), 21e-6,
                                  glm::vec3(3.426e-7, 8.298e-7, 0.356e-7), 8e3, 1.2e3, 0.758);
            skyColor = glm::clamp(skyColor, 0.0f, 1.0f);
        }
        glm::vec3 sunColor(1.0f);
        // Light color and position
        glm::vec3 lightColor(1.0f, 0.58f, 0.16f);
        glm::vec3 lightPos = glm::vec3(-4.5f, 0.0f, 1.0f) + glm::vec3(0.80644f, 5.11867f, 0.14254f);

        // Ground

        modelShader.use();

        modelShader.setMat4("viewproj", viewproj);
        modelShader.setVec3("viewPos", camera.Position);
        modelShader.setVec3("sunPos", sunPos);
        modelShader.setVec3("sunColor", sunColor);
        modelShader.setVec3("skyColor", skyColor);
        modelShader.setVec3("lightPos", lightPos);
        modelShader.setVec3("lightColor", lightColor);

        glm::mat4 mground(1.0f);
        mground = glm::translate(mground, glm::vec3(0.0f, 0.0f, 0.0f));
        modelShader.setMat4("model", mground);
        modelShader.setMat3("invModel", glm::transpose(glm::inverse(glm::mat3(mground))));
        ground.Draw(modelShader);

        glEnable(GL_CULL_FACE);

        // Models (with normal map)

        objnormShader.use();

        objnormShader.setMat4("viewproj", viewproj);
        objnormShader.setVec3("viewPos", camera.Position);
        objnormShader.setVec3("sunPos", sunPos);
        objnormShader.setVec3("sunColor", sunColor);
        objnormShader.setVec3("skyColor", skyColor);
        objnormShader.setVec3("lightPos", lightPos);
        objnormShader.setVec3("lightColor", lightColor);

        glm::mat4 mcottage(1.0f);
        mcottage = glm::translate(mcottage, glm::vec3(7.0f, 0.0f, -5.0f));
        mcottage = glm::rotate(mcottage, (float)-M_PI*0.8f, glm::vec3(0.0f, 1.0f, 0.0f));
        mcottage = glm::scale(mcottage, glm::vec3(0.5f));
        objnormShader.setMat4("model", mcottage);
        objnormShader.setMat3("invModel", glm::transpose(glm::inverse(glm::mat3(mcottage))));
        cottage.Draw(objnormShader);

        // Models (without normal map)

        modelShader.use();

        glm::mat4 mlamp(1.0f);
        mlamp = glm::translate(mlamp, glm::vec3(-4.5f, 0.0f, 1.0f));
        modelShader.setMat4("model", mlamp);
        modelShader.setMat3("invModel", glm::transpose(glm::inverse(glm::mat3(mlamp))));
        lamp.Draw(modelShader);

        glm::mat4 mbush(1.0f);
        mbush = glm::translate(mbush, glm::vec3(-8.0f, 0.0f, -8.0f));
        mbush = glm::scale(mbush, glm::vec3(0.2f));
        modelShader.setMat4("model", mbush);
        modelShader.setMat3("invModel", glm::transpose(glm::inverse(glm::mat3(mbush))));
        bush.Draw(modelShader);

        glm::mat4 mbush2(1.0f);
        mbush2 = glm::translate(mbush2, glm::vec3(12.0f, 0.0f, 4.0f));
        mbush2 = glm::scale(mbush2, glm::vec3(0.1f));
        modelShader.setMat4("model", mbush2);
        modelShader.setMat3("invModel", glm::transpose(glm::inverse(glm::mat3(mbush2))));
        bush.Draw(modelShader);

        glm::mat4 mbush3(1.0f);
        mbush3 = glm::translate(mbush3, glm::vec3(-4.0f, 0.0f, 17.0f));
        mbush3 = glm::scale(mbush3, glm::vec3(0.15f));
        modelShader.setMat4("model", mbush3);
        modelShader.setMat3("invModel", glm::transpose(glm::inverse(glm::mat3(mbush3))));
        bush.Draw(modelShader);

        glm::mat4 mbush4(1.0f);
        mbush4 = glm::translate(mbush4, glm::vec3(17.0f, 0.0f, -20.0f));
        mbush4 = glm::scale(mbush4, glm::vec3(0.15f));
        modelShader.setMat4("model", mbush4);
        modelShader.setMat3("invModel", glm::transpose(glm::inverse(glm::mat3(mbush4))));
        bush.Draw(modelShader);

        glm::mat4 mbush5(1.0f);
        mbush5 = glm::translate(mbush5, glm::vec3(0.0f, 0.0f, -22.0f));
        mbush5 = glm::scale(mbush5, glm::vec3(0.1f));
        modelShader.setMat4("model", mbush5);
        modelShader.setMat3("invModel", glm::transpose(glm::inverse(glm::mat3(mbush5))));
        bush.Draw(modelShader);

        glm::mat4 mbush6(1.0f);
        mbush6 = glm::translate(mbush6, glm::vec3(20.0f, 0.0f, 18.0f));
        mbush6 = glm::scale(mbush6, glm::vec3(0.25f));
        modelShader.setMat4("model", mbush6);
        modelShader.setMat3("invModel", glm::transpose(glm::inverse(glm::mat3(mbush6))));
        bush.Draw(modelShader);

        glm::mat4 mtree3(1.0f);
        mtree3 = glm::translate(mtree3, glm::vec3(3.0f, 0.0f, -12.0f));
        mtree3 = glm::rotate(mtree3, (float)-M_PI/2.0f, glm::vec3(1.0f, 0.0f, 0.0f));
        mtree3 = glm::scale(mtree3, glm::vec3(0.12f));
        modelShader.setMat4("model", mtree3);
        modelShader.setMat3("invModel", glm::transpose(glm::inverse(glm::mat3(mtree3))));
        tree3.Draw(modelShader);

        glm::mat4 mtree2(1.0f);
        mtree2 = glm::translate(mtree2, glm::vec3(-12.0f, 0.0f, 0.0f));
        mtree2 = glm::rotate(mtree2, (float)-M_PI/2.0f, glm::vec3(1.0f, 0.0f, 0.0f));
        mtree2 = glm::scale(mtree2, glm::vec3(0.075f));
        modelShader.setMat4("model", mtree2);
        modelShader.setMat3("invModel", glm::transpose(glm::inverse(glm::mat3(mtree2))));
        tree2.Draw(modelShader);

        glm::mat4 mtree1(1.0f);
        mtree1 = glm::translate(mtree1, glm::vec3(5.0f, 0.0f, 12.0f));
        mtree1 = glm::rotate(mtree1, (float)-M_PI/2.0f, glm::vec3(1.0f, 0.0f, 0.0f));
        mtree1 = glm::scale(mtree1, glm::vec3(0.2f));
        modelShader.setMat4("model", mtree1);
        modelShader.setMat3("invModel", glm::transpose(glm::inverse(glm::mat3(mtree1))));
        tree1.Draw(modelShader);

        // Model instanced 100x

        model100Shader.use();

        model100Shader.setMat4("viewproj", viewproj);
        model100Shader.setVec3("viewPos", camera.Position);
        model100Shader.setVec3("sunPos", sunPos);
        model100Shader.setVec3("sunColor", sunColor);
        model100Shader.setVec3("skyColor", skyColor);
        model100Shader.setVec3("lightPos", lightPos);
        model100Shader.setVec3("lightColor", lightColor);

        for (int i=0; i<numInstances; i++)
        {
            stringstream ss;
            ss << i;
            model100Shader.setVec3(("offsets[" + ss.str() + "]").c_str(), translations[i]);
        }
        mtree1 = glm::mat4(1.0f);
        mtree1 = glm::rotate(mtree1, (float)-M_PI/2.0f, glm::vec3(1.0f, 0.0f, 0.0f));
        mtree1 = glm::scale(mtree1, glm::vec3(0.2f));
        model100Shader.setMat4("model", mtree1);
        model100Shader.setMat3("invModel", glm::transpose(glm::inverse(glm::mat3(mtree1))));
        tree1.Draw(model100Shader);

        glDisable(GL_CULL_FACE);

        // Flip Buffers and Draw
        glfwSwapBuffers(mWindow);
    }

    glfwTerminate();
    return EXIT_SUCCESS;
}
